/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */
package com.resolve.team.salary.calculation.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.resolve.football.models.TeamDto;
import com.resolve.salary.calculation.SalaryCalculation;

/**
 * Salary calculation service.
 *
 * @author Rommel Medina
 *
 */
@RestController
public class SalaryCalculationController {

  @Autowired
  private SalaryCalculation<TeamDto, TeamDto> salaryService;
  
  @GetMapping("/calculation")
  public String getSalaryCalculation() {
    return "Not implemented.";
  }
  
  @PostMapping(path = "${apiconfig.basepath}/calculation", consumes = "application/json", produces = "application/json")
  public TeamDto salaryCalculation(@Validated @RequestBody TeamDto team) {
    return this.salaryService.compute(team);
  }
}
