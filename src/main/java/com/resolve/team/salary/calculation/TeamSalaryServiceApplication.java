package com.resolve.team.salary.calculation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = {
    "com.resolve.team.salary.calculation.controllers",
    "com.resolve.proportional.mappers",
    "com.resolve.proportional.definition.provider.mock",
    "com.resolve.proportional.simple",
    "com.resolve.proportional.salary.calculation"})
public class TeamSalaryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeamSalaryServiceApplication.class, args);
	}

}
