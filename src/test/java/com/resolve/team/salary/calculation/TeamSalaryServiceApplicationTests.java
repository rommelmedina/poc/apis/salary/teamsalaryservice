package com.resolve.team.salary.calculation;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.resolve.football.models.TeamDto;
import com.resolve.proportional.definition.provider.mock.MockProportionalDefinitionProviderConfiguration;
import com.resolve.proportional.mappers.ProportionalMapperConfiguration;
import com.resolve.proportional.salary.calculation.ProportionalSalaryCalculationConfiguration;
import com.resolve.proportional.simple.SimpleProportionalCalculationConfiguration;
import com.resolve.team.salary.calculation.controllers.SalaryCalculationController;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {
    SalaryCalculationControllerConfiguration.class,
    ProportionalSalaryCalculationConfiguration.class,
    SimpleProportionalCalculationConfiguration.class,
    ProportionalMapperConfiguration.class,
    MockProportionalDefinitionProviderConfiguration.class })
@WebMvcTest(controllers = SalaryCalculationController.class)
class TeamSalaryServiceApplicationTests {
  
  private final String Path = "/api/1.0/calculation";
  
  @Autowired
  private MockMvc mockMvc;
  
  @Autowired
  private ObjectMapper objectMapper;
  
  @Test
  void when_BadMethod_thenReturns405() throws Exception {
    mockMvc.perform(get(this.Path)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(405));
    
    mockMvc.perform(put(this.Path)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(405));
    
    mockMvc.perform(patch(this.Path)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(405));
    
    mockMvc.perform(delete(this.Path)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(405));
  }
  
  @Test
  void when_EmptyBody_thenReturns400() throws Exception {
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }
  
  @Test
  void when_NullBody_thenReturns400() throws Exception {
    TeamDto teamRfc = null;
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isBadRequest());
  }
  
  @Test
  void when_IncompleteBody_thenReturns400() throws Exception {
    TeamDto teamRfc = new TeamDto();
    teamRfc.setProduction(48.0);
    
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isBadRequest());
    
    teamRfc.setName("ResuelveFC");
    
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isBadRequest());
    
    teamRfc.addPlayer(null, "A", 20000.0, 7000.0, 6.0);
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isBadRequest());
    
    teamRfc.getPlayers().clear();
    teamRfc.addPlayer("Juan", null, 20000.0, 7000.0, 6.0);
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isBadRequest());
    
    teamRfc.getPlayers().clear();
    teamRfc.addPlayer("Juan", "A", null, 7000.0, 6.0);
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isBadRequest());
    
    teamRfc.getPlayers().clear();
    teamRfc.addPlayer("Juan", "A", 20000.0, null, 6.0);
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isBadRequest());
    
    teamRfc.getPlayers().clear();
    teamRfc.addPlayer("Juan", "A", 20000.0, 7000.0, null);
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isBadRequest());
    
    teamRfc.getPlayers().clear();
    teamRfc.setName(null);
    teamRfc.addPlayer("Juan", "A", 20000.0, 7000.0, 6.0);
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isBadRequest());
  }
  
  @Test
  void when_ValidInput_thenReturns200() throws Exception {
    TeamDto teamRfc = new TeamDto();
    teamRfc.setName("ResuelveFC");
    teamRfc.addPlayer("Luis", "Cuahu", 50000.0, 10000.0, 19.0);
    teamRfc.setProduction(48.0);
    
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isOk());
  }
  
  @Test
  void when_SingleCompute_thenReturns200() throws Exception {
    TeamDto teamRfc = new TeamDto();
    teamRfc.setName("ResuelveFC");
    teamRfc.addPlayer("Luis", "Cuahu", 50000.0, 10000.0, 19.0);
    teamRfc.setProduction(48.0);
    
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .accept((MediaType.APPLICATION_JSON))
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name", is("ResuelveFC")))
        .andExpect(jsonPath("$.players[0].name", is("Luis")))
        .andExpect(jsonPath("$.players[0].level", is("Cuahu")))
        .andExpect(jsonPath("$.players[0].fixedSalary", is(50000.0)))
        .andExpect(jsonPath("$.players[0].bonus", is(10000.0)))
        .andExpect(jsonPath("$.players[0].scoredPoints", is(19.0)))
        .andExpect(jsonPath("$.players[0].fullSalary", is(59550.00)))
        .andExpect(jsonPath("$.production", is(48.0)));
  }
  
  @Test
  void when_Compute_thenReturns200() throws Exception {
    TeamDto teamRfc = new TeamDto();
    teamRfc.setName("ResuelveFC");
    teamRfc.addPlayer("Luis", "Cuahu", 50000.0, 10000.0, 19.0);
    teamRfc.addPlayer("Martin", "C", 40000.0, 9000.0, 16.0);
    teamRfc.addPlayer("Pedro", "B", 30000.0, 8000.0, 7.0);
    teamRfc.addPlayer("Juan", "A", 20000.0, 7000.0, 6.0);
    teamRfc.setProduction(48.0);
    
    mockMvc.perform(post(this.Path)
        .contentType(MediaType.APPLICATION_JSON)
        .accept((MediaType.APPLICATION_JSON))
        .content(objectMapper.writeValueAsString(teamRfc)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name", is("ResuelveFC")))
        .andExpect(jsonPath("$.players[0].fullSalary", is(59550.00)))
        .andExpect(jsonPath("$.players[1].fullSalary", is(48820.0)))
        .andExpect(jsonPath("$.players[2].fullSalary", is(36640.0)))
        .andExpect(jsonPath("$.players[3].fullSalary", is(26860.0)))
        .andExpect(jsonPath("$.production", is(48.0)));
  }
}
